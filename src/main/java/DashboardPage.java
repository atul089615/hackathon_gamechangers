import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.paulhammant.ngwebdriver.ByAngular;

import java.util.List;

public class DashboardPage extends AbstractPageObject {

    public DashboardPage(WebDriver driver) {
        super(driver);
    }
    By sort =By.xpath("//div[contains(text(),'Recovered')]");
    By dataGrid =By.xpath("//div[@class='table-container']");
    By searchButtonLocator = By.className("clickable is-recovered");
    By managerLoginButton = ByAngular.buttonText("Bank Manager Login");
   

    @Override
    protected By getUniqueElement() {
        return ByAngular.buttonText("Home");
    }

    public Boolean isManagerLoginButtonDisplayed() {
        List<WebElement> searchButton = driver.findElements(managerLoginButton);
        return searchButton.size() > 0;
    }
    
    public void sortData(String column) {
    	driver.findElement(sort).click();
    }
    
    public List<String> getData() {
    	
		return null;
    	
    }
    
    
    
}
