import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.paulhammant.ngwebdriver.ByAngular;

import java.util.List;

public class LandingPage extends AbstractPageObject {

    public LandingPage(WebDriver driver) {
        super(driver);
    }

   // By searchButtonLocator = By.cssSelector("[value=\"Google Search\"]");
    By managerLoginButton = ByAngular.buttonText("Bank Manager Login");
   

    @Override
    protected By getUniqueElement() {
        return ByAngular.buttonText("Home");
    }

    public Boolean isManagerLoginButtonDisplayed() {
        List<WebElement> searchButton = driver.findElements(managerLoginButton);
        return searchButton.size() > 0;
    }
}
