import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;

public class MobileViewConfig {

    private static WebDriver driver;
    private static DesiredCapabilities capabilities;
    private static Map<String, Object> chromeOptions;
    private static Map<String, String> mobileEmulation;
    private static ChromeOptions chromeOps =new ChromeOptions();

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver_mac64");
        mobileEmulation = new HashMap<>();

        mobileEmulation.put("deviceName", "iPhone 6");
        chromeOptions = new HashMap<>();
        chromeOptions.put("mobileEmulation", mobileEmulation);

        capabilities = DesiredCapabilities.chrome();

        chromeOps =  new ChromeOptions();

        driver = new ChromeDriver();
    }
}
